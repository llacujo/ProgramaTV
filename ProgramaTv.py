

def operations(a, b, resultado):  # mirar el proyecto "pruebas" donde hay una version mejorada de este programa

    """Recibe dos numeros y un resultado,  opera entre ellos para ver si puede obtener el resultado y va guardando el
    resultado de la operaciones en una lista que luego devuelve. Si llega al resultado tambien lo devuelve
    """

    operaciones = []
    num_finales = []

    r = a+b
    operaciones.append(r)
    if r == resultado:
        num_finales = [a, b, r]

    r = a-b
    operaciones.append(r)
    if r == resultado:
        num_finales = [a, b, r]

    r =a*b
    operaciones.append(r)
    if r == resultado:
        num_finales = [a, b, r]

    r = a/b
    operaciones.append(r)
    if r == resultado:
        num_finales = [a, b, r]

    return num_finales, operaciones






def permutacion(lista, resultado):

    """
    Recibe una lista de numeros y un resultado final, permuta los numeros y realiza distintas operaciones hasta
    obtener el resultado.
    La funcion devuelve una lista de numeros, de la siguiente forma:
    num_finales = [n1, n2, n3 , n4, ....] que sigue el patron:
    n1 (+-*/) n2 = n3
    n3 (+-*/) n4 = n5 .... y continua
    """
    operaciones = []
    operaciones2 = []
    operaciones3 = []
    operaciones4 = []
    num_finales = []
    k = []
    y = []


    for n1 in lista:
        if len(k) != 0 or len(y) != 0:  # condicion de cierre
            break
        for n2 in lista:
            if len(k) != 0 or len(y) != 0:  # condicion de cierre
                break

            if n1 != n2:
                y, x = operations(n1, n2, resultado)  # la funcion "operations" realiza las permutaciones
                for i in x:
                    operaciones.append(i) # guardamos los resultados de cada iteracion en la lista 'operaciones'

    #  Hasta aqui la primera iteracion

                for o1 in operaciones:
                    if len(k) != 0 or len(y) != 0:  # condicion de cierre
                        break
                    for n3 in numeros:
                        if len(k) != 0 or len(y) != 0:  # condicion de cierre
                            break
                        if n3 != n1 and n3 != n2:
                            y, x = operations(o1, n3, resultado)

                            if len(y) != 0:  # a partir de la segunda iteracion empezamos comprobar si tenemos el resultado
                                num_finales = [n1, n2, y[0], n3]  # si lo tiene, lo guardamos en la lista y salimos
                                break

                            for i in x:
                                operaciones2.append(i)

    #  Hasta aqui la segunda

                            for o2 in operaciones2:  # repetimos el cuerpo de la funcion para el cuarto numero
                                if len(k) != 0 or len(y) != 0:  # condicion de cierre
                                    break
                                for n4 in numeros:
                                    if len(k) != 0 or len(y) != 0:  # condicion de cierre
                                        break
                                    if n4 != n1 and n4 != n2 and n4 != n3:
                                        k, x = operations(o2, n4, resultado)
                                        if len(k) != 0:  # condicion de cierre
                                            num_finales = [n1, n2, o1, n3, k[0], n4]
                                            break
                                        for i in x:
                                            operaciones3.append(i)

    #  La tercera


    #  La cuarta (se peta el ordenador) quizas se puedan meter mas numeros, si los voy sacando de la lista, por ejemplo:
    # numeros = [a, b, c, d, e] y resultado = f, lo modifico por :
    # numeros = [a, b, c, d] y resultado = f + e.


    print(num_finales)

numeros = [2, 3, 5, 7]
resultado = 210
permutacion(numeros, resultado)




def adivinaoperaciones(numeros, resultado):
    """
    Recibe una lista de numeros y un resultado que se puede obtener operando entre ellos, y devuelve los números y las
    operaciones que hay que realizar entre ellos
    """
    lista_num = permutacion(numeros, resultado)

    resul_final = [lista_num[0]]

    n1 = lista_num[0] + lista_num[1]
    n2 = lista_num[0] - lista_num[1]
    n3 = lista_num[0] * lista_num[1]


    if lista_num[2] == n1:
        resul_final.append('+')

    elif lista_num[2] == n2:
        resul_final.append('-')

    elif lista_num[2] == n3:
        resul_final.append('*')

    else:
        resul_final.append('/')

    resul_final.append(lista_num[1])

    n1 = lista_num[2] + lista_num[3]
    n2 = lista_num[2] - lista_num[3]
    n3 = lista_num[2] * lista_num[3]

    if resultado == n1:
        resul_final.append('+')

    elif resultado == n2:
        resul_final.append('-')

    elif resultado == n3:
        resul_final.append('*')

    else:
        resul_final.append('/')

    resul_final.append(lista_num[3])
    resul_final.append('=')
    resul_final.append(resultado)

    return resul_final














